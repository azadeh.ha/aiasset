import threading
import argparse
import psutil
import queue
import time
import json
import cv2
import sys
import os
import pandas as pd

sys.path.append("/app/source/Submodule name")

from bonseyes_aiasset.algorithm.algorithm import Algorithm, AlgorithmInput
from bonseyes_aiasset.utils.gstreamer_pipelines import sink_pipeline as sink_template
from bonseyes_aiasset.utils.meters import HardwareStatus


FRAME_BUFFER_SIZE = 10000
frame_queue = queue.Queue(FRAME_BUFFER_SIZE)
finished_queue = queue.Queue(1)


class FrameProducerThread(threading.Thread):
    def __init__(self, camera_device_id):
        super(FrameProducerThread, self).__init__()

        self.camera_device_id = camera_device_id
        self.capture = cv2.VideoCapture(self.camera_device_id)

    def run(self):
        while True:
            if not frame_queue.full():
                ret, frame = self.capture.read()
                if not ret:
                    break

                frame_queue.put(frame)

        return


class InferenceAndDisplayThread(threading.Thread):
    def __init__(
        self,
        frame_producer,
        deep_learning_algorithm,
        video_output,
        json_output,
        csv_output,
        debug_info,
        attach_logo
    ):
        super(InferenceAndDisplayThread, self).__init__()

        self.frame_producer = frame_producer
        self.algorithm = deep_learning_algorithm
        self.video_output = video_output
        self.json_output = json_output
        self.csv_output = csv_output
        self.debug_info = debug_info
        self.attach_logo = attach_logo
        self.json_result = {}

        self.post_processing_time = 0
        self.pre_processing_time = 0
        self.infer_time = 0

        self.cpu_usage = 0
        self.gpu_usage = 0

        self.frame_counter = 0
        self.display_time = 0
        self.algorithm_time = 0
        self.display_fps = 0
        self.algorithm_fps = 0

        self.calculate_every = 10
        self.hardware_status = HardwareStatus()

        self.sink_pipeline = sink_template % self.video_output
        self.capture_fourcc = cv2.VideoWriter_fourcc('H', '2', '6', '4')
        self.capture_fps = self.frame_producer.capture.get(cv2.CAP_PROP_FPS)
        self.capture_height = int(self.frame_producer.capture.get(cv2.CAP_PROP_FRAME_HEIGHT))
        self.capture_width = int(self.frame_producer.capture.get(cv2.CAP_PROP_FRAME_WIDTH))
        self.sink_size = (self.capture_width, self.capture_height)
        self.capture_color = True
        self.sink = cv2.VideoWriter(
            self.sink_pipeline,
            cv2.CAP_GSTREAMER,
            self.capture_fourcc,
            self.capture_fps,
            self.sink_size,
            self.capture_color,
        )
        self.logo = cv2.imread('/app/doc/logo-header.png')

    def _attach_watermark(self, rendered_frame):
        alpha = 0.5
        padding = 5

        sy = rendered_frame.shape[0] - self.logo.shape[0] - padding
        ey = sy + self.logo.shape[0]
        sx = padding
        ex = padding + self.logo.shape[1]

        added_image = cv2.addWeighted(rendered_frame[sy:ey, sx:ex, :], alpha, self.logo, 1 - alpha, 0)
        rendered_frame[sy:ey, sx:ex, :] = added_image

        return rendered_frame

    def _render_frame(self, frame, algorithm_result=None):
        if self.debug_info:
            font_color = (255, 255, 255)

            fps_infer = 'Algorithm FPS: ' + str(round(self.algorithm_fps, 1))
            cv2.putText(frame, fps_infer, (10, 20), cv2.FONT_HERSHEY_DUPLEX, 0.5, font_color, lineType=cv2.LINE_AA)
            fps_display = 'Display FPS: ' + str(round(self.display_fps, 1))
            cv2.putText(frame, fps_display, (10, 40), cv2.FONT_HERSHEY_DUPLEX, 0.5, font_color, lineType=cv2.LINE_AA)

            cpu_usage = 'CPU usage: ' + str(self.cpu_usage) + '%'
            cv2.putText(frame, cpu_usage, (10, 70), cv2.FONT_HERSHEY_DUPLEX, 0.5, font_color, lineType=cv2.LINE_AA)
            gpu_usage = 'GPU usage: %s' % str(self.gpu_usage) + '%'
            cv2.putText(frame, gpu_usage, (10, 90), cv2.FONT_HERSHEY_DUPLEX, 0.5, font_color, lineType=cv2.LINE_AA)

            input_size = 'Input size: %sx%s' % (self.algorithm.input_size, self.algorithm.input_size)
            cv2.putText(frame, input_size, (10, 120), cv2.FONT_HERSHEY_DUPLEX, 0.5, font_color, lineType=cv2.LINE_AA)

            preproc_time = 'Pre processing: ' + str(round(self.pre_processing_time, 1)) + 'ms'
            cv2.putText(frame, preproc_time, (10, 150), cv2.FONT_HERSHEY_DUPLEX, 0.5, font_color, lineType=cv2.LINE_AA)
            forward_pass_time = 'Forward: ' + str(round(self.infer_time, 1)) + 'ms'
            cv2.putText(
                frame, forward_pass_time, (10, 170), cv2.FONT_HERSHEY_DUPLEX, 0.5, font_color, lineType=cv2.LINE_AA
            )
            postproc_time = 'Post processing: ' + str(round(self.post_processing_time, 1)) + 'ms'
            cv2.putText(frame, postproc_time, (10, 190), cv2.FONT_HERSHEY_DUPLEX, 0.5, font_color, lineType=cv2.LINE_AA)

        if algorithm_result:
            rendered_frame = self.algorithm.render(frame, algorithm_result)
        else:
            rendered_frame = frame

        if self.attach_logo:
            rendered_frame = self._attach_watermark(rendered_frame)

        return rendered_frame

    def _save_frame_results(self, frame_id, algorithm_result, additional_stats):
        result = [item.dict for item in algorithm_result]
        self.json_result[frame_id] = {'prediction': result, 'stats': additional_stats}

    def _transform_json_for_csv_conversion(self, json_result):
        result = []
        for frame_id, value in json_result.items():
            predictions = value['prediction']
            for pred in predictions:
                del pred['time']
            stats = value['stats']
            item = dict(
                FRAME_ID=frame_id,
                CPU_USAGE=str(stats['cpu_usage_current']) + '%',
                GPU_USAGE=str(stats['gpu_usage_current']) + '%',
                GPU_MEMORY=str(stats['gpu_mem']) + 'GB',
                PREPROCESSING_TIME=stats['preproc_time_current'],
                INFERENCE_TIME=stats['infer_time_current'],
                POSTPROCESSING_TIME=stats['postproc_time_current'],
                TOTAL_TIME=stats['total_time'],
                PREDICTIONS=predictions,
            )
            result.append(item)

        return json.dumps(result)

    def run(self):
        while True:
            if not frame_queue.empty():
                t_display_s = time.time()
                frame = frame_queue.get()

                t_algorithm_s = time.time()
                result = self.algorithm.process(frame)
                t_algorithm_e = time.time()

                self.algorithm_time += t_algorithm_e - t_algorithm_s

                if self.frame_counter % self.calculate_every == 0 and self.frame_counter > 0:
                    self.display_fps = self.calculate_every / self.display_time
                    self.algorithm_fps = self.calculate_every / self.algorithm_time

                    hw_stats = self.hardware_status.get()
                    self.cpu_usage = hw_stats['cpu_usage']
                    self.gpu_usage = hw_stats['gpu_usage']

                    self.pre_processing_time = self.algorithm.pre_processing_time * 1000
                    self.infer_time = self.algorithm.infer_time * 1000
                    self.post_processing_time = self.algorithm.post_processing_time * 1000

                    self.algorithm_time = 0
                    self.display_time = 0

                rendered_frame = self._render_frame(frame, result)
                self.sink.write(rendered_frame)

                hw_stats = self.hardware_status.get()
                total_time = (
                    self.algorithm.pre_processing_time * 1000
                    + self.algorithm.infer_time * 1000
                    + self.algorithm.post_processing_time * 1000
                )
                additional_stats = {
                    'cpu_usage_current': hw_stats['cpu_usage'],
                    'gpu_usage_current': hw_stats['gpu_usage'],
                    'gpu_mem': hw_stats['gpu_mem_used'],
                    'preproc_time_current': self.algorithm.pre_processing_time * 1000,
                    'infer_time_current': self.algorithm.infer_time * 1000,
                    'postproc_time_current': self.algorithm.post_processing_time * 1000,
                    'total_time': total_time,
                }

                self._save_frame_results(self.frame_counter, result, additional_stats)
                cv2.imshow('Bonseyes AI Asset Camera Processing', rendered_frame)

                t_display_e = time.time()
                t_display = t_display_e - t_display_s
                self.display_time += t_display
                self.frame_counter += 1

                if cv2.waitKey(1) & 0xFF == ord('q'):
                    with open(self.json_output, 'w') as json_result:
                        json.dump(self.json_result, json_result, indent=2)

                    csv_prepared_json = self._transform_json_for_csv_conversion(self.json_result)
                    json_stats = pd.read_json(csv_prepared_json)
                    json_stats.to_csv(self.csv_output, index=False)

                    finished_queue.put(True)
                    self.frame_producer.capture.release()
                    cv2.destroyAllWindows()

                    return


def cli():
    parser = argparse.ArgumentParser(description='Bonseyes AI Asset camera processing.')
    parser.add_argument('--model', required=True, type=str, help='Path to model file')
    parser.add_argument(
        '--input-size',
        '-is',
        required=False,
        default='<default_input_size>',  # TODO add default input size
        type=str,
        help='Model input size in format: WIDTHxHEIGHT',
    )
    parser.add_argument(
        '--engine',
        nargs='?',
        const='pytorch',
        default='pytorch',
        choices=['pytorch', 'onnxruntime', 'tensorrt'],
        help='Inference engine: pytorch | onnxruntime | tensorrt',
    )
    parser.add_argument(
        '--device',
        '-de',
        nargs='?',
        const='cpu',
        default='cpu',
        choices=['cpu', 'gpu'],
        help='Available devices: cpu | gpu',
    )
    parser.add_argument(
        '--cpu-num',
        '-cn',
        required=False,
        default=None,
        type=int,
        help='Number of CPUs',
    )
    parser.add_argument(
        '--thread-num',
        '-tn',
        required=False,
        default=None,
        type=int,
        help='Number of threads',
    )
    parser.add_argument('--camera-id', default=0, type=int, help='ID of camera to be used')
    parser.add_argument(
        '--video-output',
        type=str,
        help='Path to camera recoding file',
    )
    parser.add_argument('--json-output', '-jso', required=True, type=str, help='Path to json output file')
    parser.add_argument('--csv-output', '-csvo', required=True, type=str, help='Path to csv output file')
    parser.add_argument('--debug-info', '-di', required=False, default=False, action='store_true',
                        help='Render debug info')
    parser.add_argument('--logo', '-l', required=False, default=False, action='store_true', help='Render logo')

    return parser


def main():
    args = cli().parse_args()

    if args.thread_num is not None:
        os.environ["OMP_NUM_THREADS"] = f'{args.thread_num}'

    if args.cpu_num:
        p = psutil.Process()
        if psutil.cpu_count() < args.cpu_num:
            raise Exception(
                f"Specified number of CPUs exceeds available number. "
                f"Available CPUs on machine: {psutil.cpu_count()}"
            )
        cpu_list = [*range(0, args.cpu_num)]
        p.cpu_affinity(cpu_list)

    # TODO
    # Initialize algorithm
    algorithm = Algorithm(
        model_path=args.model,
        engine_type=args.engine,
        input_size=args.input_size,
        device=args.device,
        cpu_num=args.cpu_num,
        thread_num=args.thread_num
        # + additional arguments
    )

    frame_producer_thread = FrameProducerThread(camera_device_id=args.camera_id)
    inference_and_display_thread = InferenceAndDisplayThread(
        frame_producer=frame_producer_thread,
        video_output=args.video_output,
        json_output=args.json_output,
        csv_output=args.csv_output,
        deep_learning_algorithm=algorithm,
        debug_info=args.debug_info,
        attach_logo=args.logo
    )

    frame_producer_thread.start()
    inference_and_display_thread.start()

    frame_producer_thread.join()
    inference_and_display_thread.join()

    algorithm.destroy()


if __name__ == '__main__':
    main()
