import argparse
import json

import cv2
import sys
import os

sys.path.append("/app/source/Submodule name")

from bonseyes_aiasset.algorithm.algorithm import Algorithm, AlgorithmInput


def cli():
    parser = argparse.ArgumentParser(description='Bonseyes AI Asset image processing.')
    parser.add_argument('--model', '-mo', required=True, type=str, help='Path to model file')
    parser.add_argument(
        '--input-size',
        '-is',
        required=False,
        default='<default_size>',  # TODO
        type=str,
        help='Model input size in format: WIDTHxHEIGHT',
    )
    parser.add_argument(
        '--engine',
        '-e',
        default='pytorch',
        nargs='?',
        choices=['pytorch', 'onnxruntime', 'tensorrt'],
        help='Inference engine: pytorch | onnxruntime | tensorrt',
    )
    parser.add_argument(
        '--device',
        '-de',
        nargs='?',
        const='cpu',
        default='cpu',
        choices=['cpu', 'gpu'],
        help='Available devices: cpu | gpu',
    )
    parser.add_argument(
        '--cpu-num',
        '-cn',
        required=False,
        default=None,
        type=int,
        help='Number of CPUs',
    )
    parser.add_argument(
        '--thread-num',
        '-tn',
        required=False,
        default=None,
        type=int,
        help='Number of threads',
    )
    parser.add_argument('--jpg-input', '-jpi', required=True, type=str, help='Path to jpg input file or directory')
    parser.add_argument('--jpg-output', '-jpo', required=False, type=str, help='Path to jpg output file or directory')
    parser.add_argument('--json-output', '-jso', required=False, type=str, help='Path to json output file')

    return parser


def main():
    args = cli().parse_args()

    # TODO
    # Initialize algorithm
    algorithm = Algorithm(
        model_path=args.model,
        engine_type=args.engine,
        input_size=args.input_size,
        device=args.device,
        cpu_num=args.cpu_num,
        thread_num=args.thread_num,
        # + additional arguments
    )

    inputs_ = []
    if not os.path.isdir(args.jpg_input):
        inputs_.append(args.jpg_input)
    else:
        for r, d, f in os.walk(args.jpg_input):
            for file in f:
                inputs_.append(os.path.join(r, file))

    for file_path in inputs_:
        file_name = file_path.split('/')[-1]
        input_ = os.path.abspath(file_path)

        output_jpeg = input_.replace(args.jpg_input, args.jpg_output).split('/')
        output_jpeg = '/'.join(output_jpeg[:-1]) + '/processed_%s' % file_name
        os.makedirs(os.path.dirname(output_jpeg), exist_ok=True)

        output_json = input_.replace(args.jpg_input, args.json_output).split('/')
        output_json = '/'.join(output_json[:-1]) + '/processed_%s' % file_name.replace('.jpg', '.json')
        os.makedirs(os.path.dirname(output_json), exist_ok=True)

        image = cv2.imread(input_)
        algorithm_input = AlgorithmInput(image)
        result = algorithm.process(algorithm_input)
        rendered_image = algorithm.render(image, result)

        result_json = {
            'result': [item.dict for item in result],
        }

        cv2.imwrite(output_jpeg, rendered_image)
        with open(output_json, 'w') as json_result:
            json_result.write(json.dumps(result_json, indent=2))

    algorithm.destroy()


if __name__ == '__main__':
    main()
