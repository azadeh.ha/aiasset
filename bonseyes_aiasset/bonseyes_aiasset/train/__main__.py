import argparse
import yaml
import json
import os


def cli():
    parser = argparse.ArgumentParser(description='Bonseyes Openpifpaf train script')
    parser.add_argument('--config', required=True, type=str, help='Path to config file')
    return parser


def main():
    args = cli().parse_args()
    pass


if __name__ == '__main__':
    main()
