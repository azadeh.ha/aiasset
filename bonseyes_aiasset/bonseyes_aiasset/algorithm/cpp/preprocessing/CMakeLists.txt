project(aiasset_preprocessing)

cmake_minimum_required(VERSION 3.10)


# ******************************************************************************************************************** #
#                                                   Find Packages                                                      #
# ******************************************************************************************************************** #
find_package(OpenCV REQUIRED)
MESSAGE(" *** OpenCV_INCLUDE_DIRS : " ${OpenCV_INCLUDE_DIRS})
MESSAGE(" *** OpenCV_LIB_DIRS : " ${OpenCV_LIB_DIRS})
MESSAGE(" *** OpenCV_LIBS : " ${OpenCV_LIBS})

find_package(PythonLibs 3 REQUIRED)
MESSAGE(" *** PYTHON_INCLUDE_DIRS : " ${PYTHON_INCLUDE_DIRS})
MESSAGE(" *** PYTHON_LIBRARIES : " ${PYTHON_LIBRARIES})

find_package(pybind11 CONFIG REQUIRED)
# ******************************************************************************************************************** #

# ******************************************************************************************************************** #
#                                                   Includes                                                           #
# ******************************************************************************************************************** #
include_directories(${PYTHON_INCLUDE_DIRS})
include_directories(${OpenCV_INCLUDE_DIRS})
include_directories(${OpenCV_INCLUDE_DIRS}/opencv4)
include_directories(${CMAKE_CURRENT_SOURCE_DIR})
include_directories(${pybind11_INCLUDE_DIR})
include_directories(${CMAKE_CURRENT_SOURCE_DIR})
# ******************************************************************************************************************** #

# ******************************************************************************************************************** #
#                                                 Linker Setup                                                         #
# ******************************************************************************************************************** #
LINK_DIRECTORIES(
    ${OpenCV_LIB_DIR}
)
# ******************************************************************************************************************** #

# ******************************************************************************************************************** #
#                                                Compiler Setup                                                        #
# ******************************************************************************************************************** #
SET(CMAKE_CXX_FLAGS "-std=c++11 -Wall -g -O3 -fPIC")
# ******************************************************************************************************************** #

# ******************************************************************************************************************** #
#                                             Build target - bind                                                      #
# ******************************************************************************************************************** #
if (NOT TARGET ndarray_converter)
    add_subdirectory(../deps/ndarray_converter ndarray_converter)
endif ()

file(GLOB_RECURSE SOURCES_BIND "src/*.cpp"
    "test/aiasset_preprocessor_bind.cpp"
    "../test_framework/ndarray_converter/ndarray_converter.cpp"
)
add_library(aiasset_preprocessor_bind SHARED ${SOURCES_BIND})
target_include_directories(aiasset_preprocessor_bind INTERFACE ./include)
target_link_libraries(aiasset_preprocessor_bind
    ${PYTHON_LIBRARIES}
    ${OpenCV_LIBS}
    ndarray_converter::library
)
SET_TARGET_PROPERTIES(aiasset_preprocessor_bind PROPERTIES PREFIX "")
install(TARGETS aiasset_preprocessor_bind DESTINATION ${CMAKE_CURRENT_SOURCE_DIR}/build)
# ******************************************************************************************************************** #

# ******************************************************************************************************************** #
#                                              Build target - test                                                     #
# ******************************************************************************************************************** #
if(NOT TARGET catch2)
    add_subdirectory(../deps/catch2 catch2)
endif()

if(NOT TARGET jsonl)
    add_subdirectory(../deps/jsonl jsonl)
endif()

file(GLOB_RECURSE SOURCES_TEST "src/*.cpp" "test/test_preprocessing.cpp")
add_executable(test_preprocessing ${SOURCES_TEST})
target_link_libraries(test_preprocessing
    catch2::library
    jsonl::library
    ${OpenCV_LIBS}
)

enable_testing()
add_test(test_preprocessing test_preprocessing)
# ******************************************************************************************************************** #
