import numpy as np
import os
import cv2

from bonseyes_aiasset.algorithm.algorithm import Algorithm, AlgorithmInput


def calibration_dataloader(
    size,
    images_num,
    model_path,
    dataset_path,
    engine='pytorch'
):
    print(engine)
    algorithm = Algorithm(
        model_path=model_path,
        engine_type=engine,
        input_size=size,
        device='cpu',
    )

    num = 0
    stacked = np.empty(shape=(images_num, 3, size[0], size[1]))  # NCHW input shape

    for image_name in os.listdir(dataset_path):
        image_path = f'{dataset_path}/{image_name}'
        curr_image = cv2.imread(image_path)
        preprocess_image, _ = algorithm.pre_process(curr_image)
        np_image = preprocess_image
        stacked[num] = np_image

        if num == images_num - 1:
            break

        num += 1

    algorithm.destroy()
    return stacked.astype(np.float32)


def main():
    calibration_dataloader(size=(120, 120), images_num=300)


if __name__ == '__main__':
    main()
