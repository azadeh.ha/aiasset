import argparse
import os
import sys
import torch
import torch.utils.data as data
import yaml

sys.path.append("/app/source/Submodule name")

from aimet_torch.cross_layer_equalization import equalize_model
from aimet_torch.quantsim import QuantizationSimModel
from aimet_torch.quantsim import QuantParams
from aimet_torch.utils import create_fake_data_loader
from aimet_torch import bias_correction

from bonseyes_aiasset.eval.benchmark import benchmark_pipeline, get_dataloader
from bonseyes_aiasset.algorithm.algorithm import Algorithm


class Quantize(object):
    def __init__(self, model, architecture, input_shape):
        self.input_shape = input_shape
        self.model = self.model
        self.model.eval()

    def evaluate_model(self):
        # TODO add model evaluation
        pass

    @staticmethod
    def forward_pass(model: torch.nn.Module, eval_iterations: int, use_cuda=True) -> float:
        # TODO add model forward pass
        # returning result
        pass

    def quantze_model(self, trainer_function, output_dir: str, prefix: str = 'quantised'):
        # Config file copied from AIMET:
        # aimet/TrainingExtensions/common/src/python/aimet_common/quantsim_config/default_config.json
        config_file = '/app/bonseyes_aiasset/optimize/configs/aimet_default_config.json'

        sim = QuantizationSimModel(
            self.model,
            default_output_bw=8,
            default_param_bw=8,
            dummy_input=torch.rand(self.input_shape).cuda(),
            config_file=config_file,
        )

        # Quantize the untrained model
        sim.compute_encodings(forward_pass_callback=self.forward_pass, forward_pass_callback_args=0)

        # TODO
        # Fine-tune the model's parameter using training
        # trainer_function(model=sim.model, epochs=1, num_batches=100, use_cuda=gpu)

        # Export the model
        self.model = sim.model
        sim.export(path=output_dir, filename_prefix=prefix, dummy_input=torch.rand(self.input_shape))

    def cross_layer_equalization_auto(self):
        equalize_model(self.model, self.input_shape)

    def bias_correction_empirical(self, weight_bw=8, act_bw=8):
        data_loader = get_dataloader()

        params = QuantParams(weight_bw=weight_bw, act_bw=act_bw, round_mode="nearest")

        # Perform bias correction
        bias_correction.correct_bias(
            self.model, params, num_quant_samples=1000, data_loader=data_loader, num_bias_correct_samples=512
        )

    def bias_correction_analytical_and_empirical(self, weight_bw=8, act_bw=8):
        data_loader = get_dataloader()

        # Find all BN + Conv pairs and remaining Conv from empirical BC
        module_prop_dict = bias_correction.find_all_conv_bn_with_activation(model, input_shape=self.input_shape)

        params = QuantParams(weight_bw=weight_bw, act_bw=act_bw, round_mode="nearest")

        # Perform bias correction
        bias_correction.correct_bias(
            self.model,
            params,
            num_quant_samples=1000,
            data_loader=data_loader,
            num_bias_correct_samples=512,
            conv_bn_dict=module_prop_dict,
            perform_only_empirical_bias_corr=False,
        )

    def quantize_pipeline(self, model_name, output_dir):
        # Equalize initial model
        self.cross_layer_equalization_auto()

        # Bias correction
        self.bias_correction_empirical()

        # Quantize equalized model
        self.quantze_model(trainer_function=None, output_dir=output_dir, prefix=model_name)


def cli():
    parser = argparse.ArgumentParser(description='Bonseyes AI Asset static quantization with AIMET tool.')
    parser.add_argument(
        '-m',
        '--model',
        type=str,
        required=True,
        help='Path to pre-trained model or checkpoint.',
    )
    parser.add_argument(
        '--input-size',
        '-is',
        required=False,
        default='120x120',
        type=str,
        help='Model input size in format: WIDTHxHEIGHT',
    )

    return parser


def main():
    args = cli().parse_args()

    model_repository = "/app/bonseyes_aiasset/models/pytorch/"
    if not os.path.exists(model_repository):
        os.makedirs(model_repository)

    quantize_tool = Quantize(args.model, args.arch, input_shape=(1, 3, args.input_size[1], args.input_size[0]))
    print("Starting model quantization...")

    # Evaluate inital model
    print("Evaluating initial model")
    quantize_tool.evaluate_model()

    # Equalize initial model
    print("Equalizing model")
    quantize_tool.cross_layer_equalization_auto()
    quantize_tool.evaluate_model()

    # Bias correction
    print("Bias correction")
    quantize_tool.bias_correction_empirical()
    quantize_tool.evaluate_model()

    # Quantize equalized model
    print("Quantize equalized model")
    quantize_tool.quantze_model(trainer_function=None, output_dir=model_repository, prefix='quantised')
    quantize_tool.evaluate_model()

    print('Model quantized and saved...')


if __name__ == '__main__':
    main()
