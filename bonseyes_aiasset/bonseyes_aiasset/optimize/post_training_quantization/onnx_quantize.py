import argparse
import onnxruntime

from onnxruntime.quantization import quantize_static, CalibrationDataReader, QuantFormat, QuantType

from bonseyes_aiasset.optimize.post_training_quantization.calibration_data import calibration_dataloader


class DataReader(CalibrationDataReader):
    def __init__(self, calibration_image_folder, calibration_images_num, model_path, augmented_model_path='augmented_model.onnx'):
        self.image_folder = calibration_image_folder
        self.calibration_images_num = calibration_images_num
        self.model_path = model_path
        self.augmented_model_path = augmented_model_path
        self.preprocess_flag = True
        self.enum_data_dicts = []
        self.datasize = 0

    def get_next(self):
        if self.preprocess_flag:
            self.preprocess_flag = False
            session = onnxruntime.InferenceSession(self.augmented_model_path, None)
            (_, _, height, width) = session.get_inputs()[0].shape
            nhwc_data_list = calibration_dataloader((height, width), self.calibration_images_num, self.model_path,
                                                    'onnxruntime', self.image_folder)
            input_name = session.get_inputs()[0].name
            self.datasize = len(nhwc_data_list)
            self.enum_data_dicts = iter(
                [{input_name: nhwc_data.reshape(1, 3, height, width)} for nhwc_data in nhwc_data_list]
            )
        return next(self.enum_data_dicts, None)


def cli():
    parser = argparse.ArgumentParser(description='Bonseyes AI Asset static quantization with onnxruntime tool.')
    parser.add_argument("--input-model", '-im', required=True, help="Input model path.")
    parser.add_argument("--output-model", '-om', required=True, help="Output model path.")
    parser.add_argument(
        "--calibrate-dataset",
        default="/app/bonseyes_aiasset/<calibration_data_path>",
        help="Calibration data set"
    )
    parser.add_argument(
        "--quant-format", default=QuantFormat.QOperator, type=QuantFormat.from_string, choices=list(QuantFormat),
        help="Quantization format."
    )
    parser.add_argument("--per-channel", default=False, type=bool)
    return parser


def main():
    args = cli().parse_args()
    input_model_path = args.input_model
    output_model_path = args.output_model
    data_reader = DataReader(args.calibrate_dataset, model_path=args.input_model)
    quantize_static(
        input_model_path,
        output_model_path,
        data_reader,
        quant_format=args.quant_format,
        per_channel=args.per_channel,
        weight_type=QuantType.QInt8,
    )
    print('Calibrated and quantized model saved.')


if __name__ == '__main__':
    main()
