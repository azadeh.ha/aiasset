import argparse
import os
import re
import sys
import json 

from contextlib import redirect_stdout
from torchstat import stat
from bonseyes_aiasset.algorithm.algorithm import Algorithm

sys.path.append("/app/source/Submodule name")


class ModelSummarizer:
    def __init__(self, model_path, engine, backbone, input_size):
        self.model_path = model_path
        self.engine = engine
        self.backbone = backbone
        self.input_size = input_size

    def get_model_summary(self):
        if self.engine == 'pytorch':
            return self._get_pytorch_model_summary()
        elif self.engine == 'onnxruntime':
            return self._get_onnx_model_summary()
        else:  # tensorrt
            return self._get_tensorrt_model_summary()

    def _get_onnx_model_summary(self):
        model_summary = {
            '#PARAMS': '-',
            'GFLOPS': '-',
            'memory': '-',
            'MAdd': '-',
            'MemR+W': '-'
        }
        return model_summary

    def _get_tensorrt_model_summary(self):
        model_summary = {
            '#PARAMS': '-',
            'GFLOPS': '-',
            'memory': '-',
            'MAdd': '-',
            'MemR+W': '-'
        }
        return model_summary

    def _get_pytorch_model_summary(self):
        with open('torch_summaries.txt', 'w') as f:
            # redirect stat() function output to file 'torch_summaries.txt'
            with redirect_stdout(f):
                sw = self.input_size
                sh = self.input_size

                algorithm = Algorithm(
                    model_path=self.model_path,
                    input_size=(sw, sh),
                    engine='pytorch',
                    device='cpu',
                    # **algorithm_params
                )

                print('Model', (self.model_path.split('/')[-1]).replace('.pkl', ''), 'of input size', self.input_size)
                model = algorithm.load_model()
                stat(model, (3, sw, sh))
                algorithm.destroy()

        with open("torch_summaries.txt", "r") as fp:
            lines = fp.readlines()
            for line in lines:
                # lines containing metric stats start with Total
                # e.g. "Total Flops: 13.68GFlops"
                if 'Total' in line:
                    # turn text line into a list
                    line = line.split()
                    metric = line[1].replace(':', '')
                    unit_list = re.findall("[a-zA-Z]+", line[2])
                    if unit_list:
                        unit = unit_list[0]
                    else:
                        unit = None

                    value_string = re.findall("\d+(?:[,.]\d+)*", line[2])[0]
                    value_string = re.sub(',', '', value_string)
                    value = float(value_string)
                    if metric == 'params':
                        params = value / 1000000
                    # flops in GFlops
                    if metric == 'Flops':
                        if unit == 'MFlops':
                            flops = value / 1000
                        if unit == 'GFlops':
                            flops = value
                    # memory in MB
                    if metric == 'memory':
                        if unit == 'MB':
                            memory = value
                        if unit == 'GB':
                            memory = value * float(1 << 10)
                    # multiply_add in MMAdd
                    if metric == 'MAdd':
                        if unit == 'MMAdd':
                            multiply_add = value
                        if unit == 'GMAdd':
                            multiply_add = value * 1000
                    # mem_rw in MB
                    if metric == 'MemR+W':
                        if unit == 'MB':
                            mem_rw = value
                        if unit == 'GB':
                            mem_rw = value * float(1 << 10)

        os.remove('torch_summaries.txt')
        model_summary = {
            '#PARAMS': '%.1f' % params,
            'GFLOPS': '%.1f' % flops,
            'memory': '%.1f' % memory,
            'MAdd': '%.1f' % multiply_add,
            'MemR+W': '%.1f' % mem_rw
        }
        return model_summary


def cli():
    parser = argparse.ArgumentParser(description='Bonseyes 3ddfa_V2 torchstat model summary')
    parser.add_argument(
        '--model-path',
        '-mo',
        required=True,
        type=str,
        help='Path to the model.'
    )
    parser.add_argument(
        '--engine',
        '-e',
        required=True,
        default='pytorch',
        type=str,
        help='Inference engine: pytorch | onnxruntime | tensorrt',
    )
    parser.add_argument(
        '--input-size',
        '-is',
        required=True,
        type=int,
        help='Model input size'
    )
    parser.add_argument(
        '--backbone',
        '-bb',
        nargs='?',
        const='mobilenetv1',
        default='mobilenetv1',
        choices=['mobilenetv1', 'mobilenetv0.5', 'resnet22'],   # todo change backbone choices
        help='Available backbones: mobilenetv1 | mobilenetv0.5 | resnet22',
    )
    parser.add_argument(
        '--json-output',
        required=True,
        type=str,
        help='Path to directory where json result file will be stored'
    )

    return parser


def main():
    args = cli().parse_args()
    if args.model_path.endswith('.trt') or args.engine == 'tensorrt':
        raise Exception('Tensorrt models are not supported!')

    print(f'backbone.: {args.backbone}')
    model_summarizer = ModelSummarizer(args.model_path, args.engine, args.backbone, args.input_size)
    model_summary = model_summarizer.get_model_summary()

    json_path = f'{args.json_output}/model_summaries.json'
    with open(json_path, 'w') as f:
        json.dump(model_summary, f, indent=2)


if __name__ == '__main__':
    main()
