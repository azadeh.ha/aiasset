import os
import subprocess


class TestInterfaceCpu:
    def test_interface_export_onnx(self):
        # todo change backbone option
        rc = subprocess.call(
            "python /app/interface/exporter.py --input-sizes 120x120 --engine onnxruntime --precisions fp32 --backbone <backbone>",
            shell=True)

        onnx_models = os.listdir(f'/app/bonseyes_aiasset/models/onnx/<backbone>')
        assert len(onnx_models) == 1
        assert rc == 0

    def test_interface_optimize_onnx(self):
        # todo change backbone option
        rc = subprocess.call(
            "python /app/interface/optimizer.py --input-sizes 120x120 --engine onnxruntime --backbone <backbone>",
            shell=True)

        onnx_models = os.listdir(f'/app/bonseyes_aiasset/models/onnx/<backbone>')
        assert len(onnx_models) == 2
        assert rc == 0

    def test_interface_image_onnx(self):
        # todo change backbone option
        # todo change jpg-input option
        rc = subprocess.call(
            """python /app/interface/image_processor.py \
                --input-size 120x120 \
                --engine onnxruntime \
                --backbone <backbone> \
                --device cpu \
                --jpg-input interface/tests/samples/image/<demo_image>""",
            shell=True)
        assert rc == 0

    def test_interface_image_pytorch(self):
        # todo change backbone option
        # todo change jpg-input option
        rc = subprocess.call(
            """python /app/interface/image_processor.py \
                --input-size 120x120 \
                --engine pytorch \
                --backbone <backbone> \
                --device cpu \
                --jpg-input interface/tests/samples/image/<demo_image>""",
            shell=True)
        assert rc == 0

    def test_interface_video_onnx(self):
        # todo change backbone option
        # todo change video-input option
        rc = subprocess.call(
            """python /app/interface/video_processor.py \
                --input-size 120x120 \
                --engine onnxruntime \
                --backbone <backbone> \
                --device cpu \
                --video-input interface/tests/samples/video/<demoo_video>""",
            shell=True)
        assert rc == 0

    def test_interface_video_pytorch(self):
        # todo change backbone option
        # todo change video-input option
        rc = subprocess.call(
            """python /app/interface/video_processor.py \
                --input-size 120x120 \
                --engine pytorch \
                --backbone <backbone> \
                --device cpu \
                --video-input interface/tests/samples/video/<demo_video>""",
            shell=True)
        assert rc == 0

    def test_interface_benchmark(self):
        # todo change backbone option
        # todo change dataset option
        rc = subprocess.call(
            """python /app/interface/benchmark.py \
                --input-sizes 120x120 \
                --dataset <dataset> \
                --engine pytorch onnxruntime \
                --backbone <backbone> \
                --device cpu""",
            shell=True)
        assert rc == 0
