import argparse
import subprocess
import sys
import os

CONFIG_DIR = '/app/bonseyes_aiasset/train/configs'
TRAIN_DATA_DIR = '/app/bonseyes_aiasset/data/'     # todo change train data dir
AIASSET_INFO_DIR = '/app/aiasset_info'


def cli():
    parser = argparse.ArgumentParser(description='Bonseyes AI Asset train')
    parser.add_argument('--config', '-c', required=True, type=str, help='Config file name.')
    parser.add_argument('--evaluate-model', '-vm', dest='evaluate_model', action='store_true', help='Evaluate trained model')
    parser.add_argument('--save-model', '-sm', dest='save_model', action='store_true', help='Save trained model')
    parser.add_argument('--resume', '-r', default='', type=str, metavar='PATH', help='Resume the training from a checkpoint ')

    return parser.parse_args()


def main():
    args = cli()

    if not os.path.exists(TRAIN_DATA_DIR):
        try:
            subprocess.check_call("python -m bonseyes_aiasset.data.get_data --data train", shell=True)
        except subprocess.CalledProcessError as e:
            sys.exit(e.returncode)

    if os.path.exists(AIASSET_INFO_DIR + '/train_log'):
        os.remove(AIASSET_INFO_DIR + '/train_log')

    process_id = os.getpid()
    os.makedirs(AIASSET_INFO_DIR, exist_ok=True)
    with open(AIASSET_INFO_DIR + '/train_pid', 'w') as pid_info:
        pid_info.write(str(process_id))

    evaluate_model = f'--evaluate-model' if args.evaluate_model else ''
    save_model = f'--save-model' if args.save_model else ''
    resume = f'--resume' if args.resume else ''

    try:
        subprocess.check_call(
            f"""
            python -m bonseyes_aiasset.train.train \
              --config {CONFIG_DIR}/{args.config}.yml \
              {evaluate_model} \
              {save_model} \
              {resume} \
              > {AIASSET_INFO_DIR}/train_log 2>&1 &
        """,
            shell=True,
        )
    except subprocess.CalledProcessError as e:
        sys.exit(e.returncode)


if __name__ == '__main__':
    main()
