import argparse
import os
import subprocess
import sys


def cli():
    parser = argparse.ArgumentParser(description='Bonseyes AI Asset benchmark')
    parser.add_argument(
        '--input-sizes',
        '-i', nargs='+',
        required=True,
        default=['120x120'],
        type=str,
        help='Specify target input sizes: w1xh1 w2xh2 ...'
    )
    parser.add_argument(
        '--dataset',
        '-da',
        nargs='?',
        required=False,
        default='all',
        type=str,
        choices=['aflw', 'aflw2000-3d', 'all'],
        help='Available devices: aflw | aflw2000-3d | all',
    )
    parser.add_argument(
        '--engine',
        '-e',
        required=False,
        default=['all'],
        type=str,
        nargs='+',
        choices=['pytorch', 'onnxruntime', 'tensorrt', 'all'],
        help='Choose one or more engines: pytorch | onnxruntime | tensorrt | all',
    )
    parser.add_argument(
        '--backbone',
        '-bb',
        nargs='?',
        const='mobilenetv1',
        default='mobilenetv1',
        choices=['mobilenetv1', 'mobilenetv0.5', 'resnet22'],   # todo change backbone choices
        help='Available backbones: mobilenetv1 | mobilenetv0.5 | resnet22',
    )
    parser.add_argument(
        '--device',
        '-de',
        nargs='?',
        const='cpu',
        default='cpu',
        choices=['cpu', 'gpu'],
        help='Available devices: cpu | gpu',
    )

    return parser.parse_args()


def main():
    args = cli()

    if not os.path.exists('/app/bonseyes_aiasset/data/'):     # todo change the dataset path
        try:
            subprocess.check_call("python -m bonseyes_aiasset.data.get_data --data test", shell=True)
        except subprocess.CalledProcessError as e:
            sys.exit(e.returncode)

    input_sizes = ' '.join((ins.split("x")[0]) for ins in args.input_sizes)
    engine = ' '.join(eng for eng in args.engine)

    try:
        subprocess.check_call(
            f"""
            python -m bonseyes_aiasset.benchmark.all \
              --input-sizes {input_sizes} \
              --dataset {args.dataset} \
              --engine {engine} \
              --backbone {args.backbone} \
              --device {args.device}
        """,
            shell=True,
        )
    except subprocess.CalledProcessError as e:
        sys.exit(e.returncode)


if __name__ == '__main__':
    main()
