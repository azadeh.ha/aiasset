import argparse
import subprocess
import sys

from bonseyes_aiasset.utils.meters import HardwareStatus


def cli():
    parser = argparse.ArgumentParser(description='Bonseyes AI Asset video demo')
    parser.add_argument(
        '--input-size',
        '-is',
        required=False,
        default='120x120',
        type=str,
        help='Model input size in format: WIDTHxHEIGHT',
    )
    parser.add_argument(
        '--engine',
        '-en',
        nargs='?',
        const='pytorch',
        default='pytorch',
        choices=['pytorch', 'onnxruntime', 'tensorrt'],
        help='Inference engine: pytorch | onnxruntime | tensorrt',
    )
    parser.add_argument(
        '--backbone',
        '-bb',
        nargs='?',
        const='mobilenetv1',
        default='mobilenetv1',
        choices=['mobilenetv1', 'mobilenetv0.5', 'resnet22'],   # todo change backbone choices
        help='Available backbones: mobilenetv1 | mobilenetv0.5 | resnet22',
    )
    parser.add_argument(
        '--device',
        '-de',
        nargs='?',
        const='cpu',
        default='cpu',
        choices=['cpu', 'gpu'],
        help='Available devices: cpu | gpu',
    )
    parser.add_argument(
        '--precision',
        required=False,
        nargs='?',
        const='fp32',
        default='fp32',
        choices=['fp32', 'fp16', 'int8'],
        help='Model precision: fp32, fp16. Default: fp32'
    )
    parser.add_argument(
        '--version',
        '-v',
        nargs='?',
        const='v1.0',
        default='v1.0',
        choices=['v1.0'],
        help='AI Asset version.'
    )
    parser.add_argument(
        '--cpu-num',
        '-cn',
        required=False,
        default=None,
        type=int,
        help='Number of CPUs',
    )
    parser.add_argument(
        '--thread-num',
        '-tn',
        required=False,
        default=None,
        type=int,
        help='Number of threads',
    )
    parser.add_argument(
        '--single-face-track',
        '-sft',
        required=False,
        default=False,
        dest='single_face_track',
        action='store_true',
        help="Track single face.",
    )
    parser.add_argument(
        '--safety-check',
        '-sc',
        required=False,
        type=int,
        default=0,
        choices=range(0, 51),
        metavar="[0-50]",
        help="Safety face detection check on frame interval [0-50] frames.",
    )
    parser.add_argument("--width", '-wi', required=False, help="Width in pixels.")
    parser.add_argument("--height", '-he', required=False, help="Height in pixels.")
    parser.add_argument('--rotate', '-r', required=False, type=int, help='Rotate video frames: 90, -90, 180')
    parser.add_argument("--color", '-co', required=False, help="Color format: GRAY | BGR.")
    parser.add_argument('--video-input', '-vi', required=True, type=str, help='Path to video input file or directory')

    return parser.parse_args()


def main():
    args = cli()

    if args.color == 'BGR':
        color = ''
    elif args.color == 'GRAY':
        color = '-ctg'
    else:
        color = ''

    input_size = args.input_size.split('x')[0]

    cpu_num = f"--cpu-num {args.cpu_num}" if args.cpu_num else ''
    thread_num = f"--thread-num {args.thread_num}" if args.thread_num else ''

    width = f"--resize-width {args.width}" if args.width else ''
    height = f"--resize-height {args.height}" if args.height else ''
    rotate = f"--rotate {args.rotate}" if args.rotate else ''

    if thread_num == '':
        thread_num_env_var = 'OMP_NUM_THREADS=1'
    else:
        thread_num_env_var = 'OMP_NUM_THREADS=%s' % args.thread_num

    hws = HardwareStatus()
    gpu_name = hws.get()['gpu_name']

    model_path = '/app/bonseyes_aiasset/models'

    if args.engine == 'pytorch':
        model_path += f'/{args.engine}'
        model_path += f'/{args.backbone}'
        model_path += f'/{args.version}_{args.backbone}_default_{args.input_size}_fp32.pth'
    elif args.engine == 'onnxruntime':
        model_path += '/onnx'
        model_path += f'/{args.backbone}'
        model_path += f'/{args.version}_{args.backbone}_default_{args.input_size}_{args.precision}.onnx'
    elif args.engine == 'tensorrt':
        model_path += f'/{args.engine}'
        model_path += f'/{gpu_name}'
        model_path += f'/{args.backbone}'
        model_path += f'/{args.version}_{args.backbone}_default_{args.input_size}_{args.precision}_dla_disabled.trt'

    try:
        subprocess.check_call(f"""
            {thread_num_env_var} python -m bonseyes_aiasset.process.video \
              --model "{model_path}" \
              --input-size {input_size} \
              --engine {args.engine} \
              --backbone {args.backbone} \
              --device {args.device} \
              {cpu_num} \
              {thread_num} \
              {rotate} \
              {color} \
              {width} \
              {height} \
              {'--single-face-track' if args.single_face_track else ''} \
              --video-input /app/{args.video_input} \
              --video-output /app/ \
              --json-output /app/ \
              --csv-output /app/        
        """, shell=True)
    except subprocess.CalledProcessError as e:
        sys.exit(e.returncode)


if __name__ == '__main__':
    main()
